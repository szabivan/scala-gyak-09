package task05

import org.scalatest.concurrent.{Signaler, TimeLimitedTests}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.time.{Millis, Span}

import scala.util.Random

class MapTest extends AnyFlatSpec with TimeLimitedTests {
  val timeLimit: Span = Span(1000, Millis)
  override val defaultTestSignaler: Signaler = new Signaler {
    override def apply(testThread: Thread): Unit = {
      println("Ez a teszt túl sokáig fut.")
      testThread.stop()
    } //unsafe, never használd.
  }

  "MapTest" should "for a JSONString" in {
    val input = JSONString("tibi")
    val result = input.map(_.length)
    val expected = JSONString("4")
    assert(result == expected, s"$input.countStrings eredménye $result lett, $expected kéne legyen!")
  }

  it should "for an array of JSONStrings" in {
    val input = JSONArray(Vector(JSONString("feri"), JSONString("robi"), JSONString("zoli")))
    val result = input.map(_.length)
    val expected = JSONArray(Vector(JSONString("4"), JSONString("4"), JSONString("4")))
    assert(result == expected, s"$input.countStrings eredménye $result lett, $expected kéne legyen!")
  }

  it should "for a complex JSON Object" in {
    val input = JSONObject(Vector(("name",JSONString("feri")), ("age", JSONString("42")), ("data", JSONObject(Vector(("field",JSONString("none")),("lottery",JSONArray(Vector(JSONString("13"),JSONString("42")))))))))
    val result = input.map(_.length)
    val expected = JSONObject(Vector(("name",JSONString("4")), ("age", JSONString("2")), ("data", JSONObject(Vector(("field",JSONString("4")),("lottery",JSONArray(Vector(JSONString("2"),JSONString("2")))))))))
    assert(result == expected, s"$input.countStrings eredménye $result lett, $expected kéne legyen!")
  }

}