package task06

import org.scalatest.concurrent.{Signaler, TimeLimitedTests}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.time.{Millis, Span}

class WorkspaceStatsTest extends AnyFlatSpec with TimeLimitedTests {
  val timeLimit: Span = Span(1000, Millis)
  override val defaultTestSignaler: Signaler = new Signaler {
    override def apply(testThread: Thread): Unit = {
      println("Ez a teszt túl sokáig fut.")
      testThread.stop()
    } //unsafe, never használd.
  }

  val (sanyi, jozsi, feri, tibi) =
    (Ember("Sándor","sanyi",1972), Ember("József", "joci", 1980), Ember("Ferenc", "kedves", 2000), Ember("Sándor", "tibi", 1998))
  val (theIT, keksz, vodka) =
    (Osztaly("IT"), Osztaly("Kekszgyár"), Osztaly("Vodkagyár"))

  "osztalymeret" should "konstansokra" in {
    val input = Map(sanyi -> theIT, jozsi -> keksz, feri -> theIT)
    assert( WorkspaceStats.osztalyMeretek(input) == Map(theIT -> 2, keksz -> 1), s"hibás osztályméretek a következő inputra: $input")
  }
  "mennyitKeresnek" should "konstansokra" in {
    val inputOsztalyok = Map(sanyi -> theIT, jozsi -> keksz, feri -> theIT, tibi -> keksz)
    val inputFizetesek = Map(sanyi -> 100, jozsi -> 60, feri -> 30, tibi -> 60)
    assert( WorkspaceStats.mennyitKeresnek(inputOsztalyok, inputFizetesek)
    == Map(theIT -> 130, keksz -> 120), s"hibás fizetésösszegzés erre: $inputOsztalyok, $inputFizetesek")
  }
  "loginhozEmbert" should "konstansokra" in {
    val input = Vector(sanyi, jozsi, feri, tibi)
    assert( WorkspaceStats.loginhozEmbert(input) ==
    Map(sanyi.login -> sanyi, jozsi.login -> jozsi, feri.login -> feri, tibi.login -> tibi),
      s"hibás loginhoz mappelés erre: $input")
  }
  "dolgozokBonuszSzerint" should "konstansokra" in {
    val input = Vector((jozsi,4),(tibi,3),(jozsi,2),(feri,5),(sanyi,2),(tibi,1),(sanyi,2))
    val expected1 = Vector((jozsi, 6), (feri,5), (tibi,4), (sanyi,4))
    val expected2 = Vector((jozsi, 6), (feri,5), (sanyi,4), (tibi,4))
    val result = WorkspaceStats.dolgozokBonuszSzerint(input).toVector
    assert( result == expected1 || result == expected2, s"hibás bónusz szerinti csökkenőbe rendezés erre: $input")
  }

}
