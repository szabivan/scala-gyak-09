package task03

import org.scalatest.concurrent.{Signaler, TimeLimitedTests}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.time.{Millis, Span}

class HuffmanTest extends AnyFlatSpec with TimeLimitedTests {
  val timeLimit: Span = Span(1000, Millis)
  override val defaultTestSignaler: Signaler = new Signaler {
    override def apply(testThread: Thread): Unit = {
      println("Ez a teszt túl sokáig fut.")
      testThread.stop()
    } //unsafe, never használd.
  }

  "FreqCount" should "count the letters" in {
    val expected =
    Map('n' -> 129, 'j' -> 3, 't' -> 173, 'F' -> 1, ' ' -> 337, 'V' -> 2, 'P' -> 15, 'p' -> 45,
      'C' -> 2, 'r' -> 128, 'D' -> 5, 'E' -> 1, 'e' -> 233, 's' -> 161, 'x' -> 7, '.' -> 43, 'N' -> 2,
      'u' -> 191, 'f' -> 20, 'A' -> 2, 'a' -> 158, 'm' -> 83, 'M' -> 3, 'I' -> 1, 'i' -> 216,
      ',' -> 35, 'v' -> 31, 'q' -> 33, 'Q' -> 4, 'L' -> 1, 'b' -> 18, 'g' -> 18, 'l' -> 136, 'c' -> 106,
      'h' -> 9, 'o' -> 84, 'S' -> 4, 'd' -> 47)
    assert(Huffmann.freqs(Huffmann.text) == expected, "A Lorem Ipsum texten nem az elvárt Mapet kaptuk!")
  }
}
