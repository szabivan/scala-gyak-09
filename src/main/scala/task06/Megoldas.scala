package task06

import scala.collection.{SortedSet, StringOps}

object Megoldas extends App {
  /** Írjunk függvényt, mely kap egy Map[Ember, Osztaly]-t, ami tárolja, hogy melyik emberünk
   *  melyik osztályon dolgozik, és visszaad egy Map[Osztaly, Int]-et, ami minden osztályhoz
   *  hozzárendeli, hogy ott hányan dolgoznak!
   */
  def osztalyMeretek(holDolgozik: Map[Ember, Osztaly]): Map[Osztaly, Int] =
    holDolgozik.groupMapReduce(_._2)(_ => 1)(_ + _)

  /** Írjunk függvényt, mely kap egy Map[Ember, Osztaly]-t, ami tárolja, hogy melyik emberünk
   *  melyik osztályon dolgozik, és egy Map[Ember,Int]-et, ami pedig azt, hogy melyik emberünk
   *  mennyit keres, és visszaad egy Map[Osztaly, Int]-et, ami visszaadja, hogy az egyes osztályokon
   *  az emberek összes fizetése mennyi!
   *  Feltehető, hogy a két bejövő Mapben a kulcsok halmaza (az emberek) ugyanaz.
   */
  def mennyitKeresnek(holDolgozik: Map[Ember, Osztaly], fizetes: Map[Ember, Int]): Map[Osztaly, Int] =
    holDolgozik.groupMapReduce(_._2)(pair => fizetes(pair._1))(_ + _)

  /** Írjunk függvényt, mely kap egy Vector[Ember]-t, és elkészít egy Map[String,Ember]-t, ami tárolja,
   *  hogy melyik login azonosítóhoz melyik emberünk tartozik! Feltehetjük, hogy az inputban érkező embereknek
   *  páronként különbözik a loginja (a nevük lehet egyforma).
   */
  def loginhozEmbert(emberek: Vector[Ember]): Map[String, Ember] =
    emberek
      .map(ember => (ember.login, ember))
      .toMap

  /** Írjunk függvényt, mely kap egy Vector[(Ember, Int)]-et azzal, hogy melyik emberünk mennyi bónuszt kapott
   * (egy ember többször is kaphat bónuszt), és adjunk vissza egy SortedSet[(Ember,Int)]-et, amiben
   * - minden emberhez a neki összesített bónusz tartozik,
   * - és eszerint a bónusz szerint vannak csökkenőbe rendezve!
   * Pl. Vector((jozsi,4),(tibi,3),(jozsi,2),(feri,5),(tibi,1)) inputra a sorted set
   * első eleme (jozsi,6), második (feri,5), harmadik pedig (tibi,4) legyen.
    */
  def dolgozokBonuszSzerint(bonuszok: Vector[(Ember,Int)]): SortedSet[(Ember,Int)] =
    SortedSet[(Ember,Int)]()((pair1, pair2) =>
      if (pair1._2 != pair2._2) pair2._2 - pair1._2 // így lesz csökkenőben
      else pair1._1.login.compareTo(pair2._1.login) // mindegy, csak breakeljük a tiet, a login meg unique
    ) ++
    bonuszok
      .groupMapReduce(pair => pair._1)(_._2)(_ + _)
      .toVector
}