package task05
/*
 * (Egyszerű) JSON elementeket tárolunk.
   Egy JSONElement lehet
   - egy JSONString, amiben egy String adattag van
   - egy JSONArray, amiben JSONElement-ek egy vektora van
   - egy JSONObject, amiben (key:String, element:JSONElement) párok vektora van
 * Implementáljuk a trait-ben megadott metódusokat! Célszerű lehet ezeket overrideolni a három case classban, de nem kötelező.

   Pl. egy JSONElement, amin a példák specifikálva lesznek:
   JSONObject(Vector(("name",JSONString("sanyi")),("data",JSONArray(Vector(JSONString("feri"), JSONString("joci"))))))
 **/
object Megoldas {

trait JSONElement {
  /** Adjuk vissza, hogy összesen hány String szerepel a JSONElementünkben, úgy is mint JSONString adattag
   * és úgy is, mint JSONObject key!
   * A példa JSONElementre pl. ennek 5-öt kéne visszaadnia.
   **/
  def countStrings: Int

  /** A map metódus kapjon egy f: String => B metódust, és adjon vissza egy új JSONElementet, melynek a JSONString-ek adattagjaira
   * alkalmazzuk az f függvényt, majd az eredmény toString-je kerüljön bele az új JSONString-be adattagként!
   * A példa JSONElementre pl. a map( _.length )exit hívás eredménye
   * JSONObject(Vector(("name",JSONString("5")),("data",JSONArray(Vector(JSONString("4"), JSONString("4"))))))
   * kéne legyen.
   **/
  def map[B](f: String => B): JSONElement

  /** A filter metódus kapjon egy p: String => Boolean predikátumot, és adjon vissza egy olyan új JSONElement-et, ami
   * az eredetitől annyiban tér el, hogy a benne levő összes JSONObject-nek tűnjön el az összes olyan attribútuma,
   * melyek kulcsára p hamis lesz!
   * A példa JSONElementre pl. a filter( _.startsWith("na") ) eredménye
   * JSONObject(Vector(("name",JSONString("sanyi"))))
   * kéne legyen.
   */
  def filter(p: String => Boolean): JSONElement

  /** A count metódus kapjon egy p: JSONElement => Boolean predikátumot, és adja vissza, hogy a JSONElement-tel és az alatta levő
   *  összessel együtt mennyi olyan van közülük, melyre igaz a p!
   *  A példa JSONElementre pl. a count( _.countStrings % 2 == 1 ) eredménye 4 kéne legyen.
   */
  def count(p: JSONElement => Boolean): Int

}

case class JSONString(value: String) extends JSONElement {
  override def countStrings: Int = 1
  override def map[B](f: String => B): JSONElement = JSONString(f(value).toString)
  override def filter(p: String => Boolean): JSONElement = this
  override def count(p: JSONElement => Boolean): Int = if (p(this)) 1 else 0
}
case class JSONArray(data: Vector[JSONElement]) extends JSONElement {
  override def countStrings: Int = data.map(_.countStrings).sum
  override def map[B](f: String => B): JSONElement = JSONArray(data.map(_.map(f)))
  override def filter(p: String => Boolean): JSONElement = JSONArray(data.map(_.filter(p)))
  override def count(p: JSONElement => Boolean): Int = data.map(_.count(p)).sum + (if (p(this)) 1 else 0)
}
case class JSONObject(attributes: Vector[(String, JSONElement)]) extends JSONElement {
  override def countStrings: Int = attributes.length + attributes.map(_._2.countStrings).sum
  override def map[B](f: String => B): JSONElement = JSONObject(attributes.map(pair => pair._1 -> pair._2.map(f)))
  override def filter(p: String => Boolean): JSONElement =
    JSONObject(attributes.filter(pair => p(pair._1)).map(pair => (pair._1, pair._2.filter(p))))
  override def count(p: JSONElement => Boolean): Int = attributes.map(_._2.count(p)).sum + (if (p(this)) 1 else 0)
}

}