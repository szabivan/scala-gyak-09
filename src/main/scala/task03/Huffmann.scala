package task03

object Huffmann extends App {
  //Huffmann-kódolni fogunk: egy input stringből felépítünk egy szótárat,
  // a gyakoribb betűk rövidebb bináris kódszavakat kapnak, és ezzel a szótárral el-,
  // majd visszakódoljuk a stringünket. Részleteket lásd lejjebb.

  //Először, ha kapunk egy kódolandó String-et, megszámoljuk, hogy melyik karakterből mennyi van belőle.
  def freqs(text: String): Map[Char, Int] = ???

  //A Huffmann-kódolásban bináris (de nem kereső!) fában tároljuk a szótárat: minden fapontban van egy
  //érték is, hogy milyen "nehéz", a belső csúcsoknak mindig pontosan két gyereke van, a zero és a one,
  //ők szintén HuffmanTree-k, a levelekben nincs gyerek, de a súlyon felül
  //van egy karakter infó is, hogy melyik karaktert kódolja ez a fapont.
  sealed trait HuffmanTree {
    def weight: Int
  }
  case class HuffmanLeaf(char: Char, weight: Int) extends HuffmanTree
  case class HuffmanInner(zero: HuffmanTree, weight: Int, one: HuffmanTree) extends HuffmanTree

  //A Huffmann-kódolásban kiindulva a betű gyakoriság mapból,
  // először is minden betű-gyakoriság párból készítünk
  //egy-egy egypontú Huffman-fát, és ezeket egy olyan Map-ba tesszük, aminek a súly a kulcsa, és
  //minden súly kulcshoz az olyan súlyú HuffmanTreek vektora tartozik mint érték.
  def freqsToLeaves(freqMap: Map[Char,Int]): Map[Int, Vector[HuffmanLeaf] ] = ???

   //egy helper metódus: a removeMin egy Map[Int,Vector[HuffmanTree]]-ből visszaad egy minimális kulcshoz tartozó
  //vektor-beli HuffmanTree-t, és a módosított Map-ot, melyből ez a tree ki van véve. Ha nincs ilyen (mert nincs több
  //nemüres Vector a values-ban), akkor None-t ad vissza tree-ként és változatlanul hagyja a Map-ot.
  def removeMin(freqMapTree: Map[Int,Vector[HuffmanTree]]):
    (HuffmanTree, Map[Int, Vector[HuffmanTree]]) = ???

  //A szótárépítő algoritmus:
  //- kiindulunk egy Map[Int,Vector[HuffmanTree]]-ből, melyben az előző metódus szerint elkódolt
  //  HuffmanTree-k vannak (gyökérelemekben tárolt súlyokkal)
  //- ha már csak egyetlen HuffmanTree van a Mapban, visszaadjuk azt
  //- különben, kiveszünk két minimális súlyút a Mapból, készítünk belőlük egy újat, melynek
  //---súlya a két fa összsúlya,
  //---a két gyereke pedig a két kivett fa,
  //---majd ezt visszatesszük a Mapbe a másik kettő helyett.
  //-Mindezt addig ismételjük, amíg már csak egy fa marad a Mapben és ezt visszaadjuk.
  def collapseTrees(freqMapTree: Map[Int, Vector[HuffmanTree]]): HuffmanTree = ???

  //A kódoló algoritmusban a szótár HuffmanTree-ből építünk egy Char=>String Mapet,
  //ha pl. az 'a' karaktert tároló levélhez a zero, one, zero, zero irányba kell mennünk,
  //akkor az 'a' kódja "0100" lesz.
  def huffmanTreeToMap(codeTree: HuffmanTree): Map[Char, String] = ???

  //Ha van egy Map[Char,String]-ünk, melyben minden Char-nak megvan egy (bináris string) kódja,
  //minden karaktert lecserélhetünk a kódjára és kapunk egy (hosszú) bináris stringet.
  def encodeToBinaryString(text: String, code: Map[Char, String]): String = ???

  //Ha van egy bináris stringünk, készíthetünk belőle egy byte tömböt úgy, hogy minden 8 bitnyi stringből egy
  //0-255 közti Byteot építünk (a végét padoljuk ki 0-kkal, hogy 8-cal osztható legyen a hossza)
  //a String drop(n) metódusa eldobja a string első n karakterét és visszaadja a suffixet;
  //a String take(n) metódusa csak az első n karakteréből álló stringet adja vissza.
  def compressBinaryString(text: String): Vector[Byte] = ???

  //írjuk meg az előzőeket felhasználva a huffmann-kódoló metódust: inputja a szöveg,
  //outputja az elkódolt eredmény (Vector[Byte]) és a két szótár (a HuffmanTree és a Char=>String map)!
  def huffmanEncode(text: String): (Vector[Byte], HuffmanTree, Map[Char,String]) = ???

  //írjunk függvényt, ami egy byte-ból egy 8 hosszú bináris stringet készít!
  def decodeByte( byte: Byte ) : String = ???

  //írjuk meg a dekódert is, ami ezt a három objektumot, plusz az eredeti string hosszát kapja meg (miért is?)
  //és visszakódolja az eredeti stringet!
  def huffmanDecode(bytes: Vector[Byte], codeTree: HuffmanTree, codeMap: Map[Char,String], length: Int ): String = ???

  //teszteljük lorem ipsummal. Ha minden jól ment, akkor ez a 2487 karakteres szöveg egy 1319 byteos folyamba tömörödik.
  def text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. " +
    "Vestibulum mattis arcu quam, at facilisis ligula consectetur nec. Pellentesque quis posuere ipsum. " +
    "Sed tristique condimentum leo, id sollicitudin odio facilisis et. Aliquam viverra diam id aliquam consequat. " +
    "Quisque suscipit nulla eu venenatis ultricies. Duis pulvinar nisi ac pulvinar aliquet. Sed sed nibh nisl." +
    "Pellentesque ornare leo orci, ac pretium justo tincidunt a." +
    "Curabitur viverra est ac turpis faucibus, vel cursus leo consequat." +
    "Sed eleifend, urna ac imperdiet semper, orci turpis ornare urna, in vestibulum eros nulla id libero." +
    "Quisque felis lacus, venenatis at sagittis vitae, euismod nec ipsum." +
    "Pellentesque fermentum, neque laoreet dictum semper, metus orci semper nunc, ac placerat enim lorem placerat ligula." +
    "Maecenas feugiat elit enim, at rutrum nisl ornare placerat. Donec volutpat, augue quis rhoncus dignissim," +
    "justo mauris tempor nisl, at tincidunt mauris libero at nulla. Quisque finibus nulla eu risus vulputate dignissim." +
    "Aliquam maximus libero ut nisl efficitur, eget porttitor sapien consequat. Proin sollicitudin lectus a sodales lobortis." +
    "Proin volutpat rutrum neque, at blandit libero aliquam nec." +
    "Pellentesque ex lectus, semper a lectus vel, mollis iaculis lacus. Donec ultricies mauris ac tincidunt mollis." +
    "Etiam mattis ex sit amet dui ultricies gravida ut mattis tellus." +
    "Donec leo ligula, lacinia eget consectetur vel, faucibus vitae nunc. Proin luctus vulputate scelerisque." +
    "Praesent rhoncus justo a diam accumsan tincidunt." +
    "Phasellus efficitur, orci sodales convallis porttitor, purus mi sollicitudin nisl, nec rhoncus diam elit eget ex." +
    "Nulla imperdiet ac ex quis imperdiet. Cras sit amet pulvinar leo. " +
    "Praesent sagittis feugiat nibh, viverra eleifend velit rutrum id. Maecenas rutrum lectus ultrices ipsum convallis mattis." +
    "Quisque luctus congue tortor vel pretium. Praesent elementum tincidunt tempus." +
    "Nullam lacinia, ante non pharetra iaculis, ipsum odio rutrum urna, consequat consequat neque eros quis elit." +
    "Fusce imperdiet tellus nec felis accumsan maximus. Proin nulla est, blandit non lacus ut, accumsan tincidunt dolor." +
    "Donec pretium quam ligula, nec tempor erat placerat eu. Praesent efficitur scelerisque nunc ut ultricies. Sed nec varius erat." +
    "Proin volutpat purus ultrices vestibulum finibus. Praesent pretium venenatis quam, id pharetra diam." +
    "Integer in arcu ac eros ornare scelerisque sit amet non lectus. Maecenas aliquet dictum laoreet." +
    "Vivamus eleifend sem nibh, ac maximus urna tempus ultrices."

  val (byteVector, codeTree, codeMap) = huffmanEncode(text)
  println(byteVector)
  println(codeTree)
  println(codeMap)
  println("Eredeti hossz: " + text.length + ", kódolt hossz: " + byteVector.length )

  val temp = huffmanDecode(byteVector, codeTree, codeMap,text.length)
  println(temp)
}
